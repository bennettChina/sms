<%--
  Created by IntelliJ IDEA.
  User: John
  Date: 2019/7/4
  Time: 18:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © layui.com - 底部固定区域
    </div>
</div>
<script src="/static/layui/layui.js"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use(['element','form','layer','laypage', 'layedit', 'laydate', 'upload', 'table'], function(){
        var layedit = layui.layedit,
            upload = layui.upload,
            form = layui.form,
            layer = layui.layer,
            laypage = layui.laypage,
            table = layui.table;

        //创建一个编辑器
        var editIndex = layedit.build('edit_message_content');

        //拖拽上传
        upload.render({
            elem: '#attachment'
            // ,url: '/message/send'
            ,accept: 'file'
            ,field:'myFile'
            ,auto: false //选择文件后不自动上传
            ,bindAction: '#submitBtn'
            ,before:function(obj) {
                // this.data=$("#sendMessageForm").serializeObject(); //根据表单的id获取数据
                var files = obj.pushFile();
            }
            ,choose: function(obj){ //选择文件后预览
                //将每次选择的文件追加到文件队列
                var files = obj.pushFile();
                //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
                obj.preview(function(index, file, result){
                    console.log(index); //得到文件索引
                    console.log(file); //得到文件对象
                    console.log(result); //得到文件base64编码，比如图片
                    $("#attachment p").html("<li>"+file.name+"</li>");
                });
            }
        });

        // 通过layui表格插件动态加载数据
        table.render({
            elem: '#receiveMsgTable'
            // ,height: 312
            ,url: '/message/load_receive_msg_layui' //数据接口
            // 由于后台的分页参数和LayUI默认的参数名不一样，我们需要指定一下
            ,request: {
                pageName: 'pageNum', //页码的参数名称，默认：page
                limitName: 'pageSize' //每页数据量的参数名，默认：limit
            }
            // 设置默认pageSize和可供用户选择的pageSize
            ,page: {
                limit: 5,
                limits: [2, 5, 10]
            }
            ,cols: [[ //表头
                {field: 'id', title: 'ID', width:80, sort: true, fixed: 'left'}
                ,{field: 'status', title: '状态', width:80, templet: formatMsgStatus}
                ,{field: 'subject', title: '标题', sort: true, templet: formatMsgSebject}
                ,{field: 'operation', title: '操作', width:160, templet: formatOp}
                ,{field: 'createtime', title: '时间', width: 160}
            ]]
        });

        function formatMsgStatus(obj) {
            if(obj.status == '1') {
                return "<img src=\"/static/images/sms_readed.png\" />";
            } else if(obj.status == '2') {
                return "<img src=\"/static/images/sms_unReaded.png\" />";
            }
        }

        function formatMsgSebject(obj) {
            return "<a href=\"/message/to_read/"+obj.id+"/receive\">"+obj.subject+"</a>";
        }

        function formatOp(obj) {
            return "<a href=\"javascript:del_msg_layui("+obj.id+")\">删除</a> &nbsp;&nbsp;<a href=\"#\">回信</a>";
        }

        window.del_msg_layui = function(id) {
            del_msg(id, function () {
                table.reload("receiveMsgTable");
            });
        }

        window.del_msg = function(id, refreshCallback) {
            layer.open({
                type: 1
                ,title: '温馨提示'
                ,content: '<p style="padding: 5px 10px;">确认删除吗？</p>'
                ,shade: false //不显示遮罩
                ,btn: ['确定', '取消']
                ,yes: function(index){
                    console.log("yes");
                    $.getJSON('/message/del', {"id": id}, function(jsonData){
                        var code = jsonData.code;
                        if(code == '0') {
                            // 删除成功后，立即重新load数据
                            if(refreshCallback) {
                                refreshCallback();
                            } else {
                                loadReceiveMsg();
                            }
                        } else {
                            // 删除失败
                            layer.alert("删除失败");
                        }
                    });

                    //如果设定了yes回调，需进行手工关闭
                    layer.close(index);
                },btn2: function() {
                    console.log("no");
                }
            });
        }
    });
</script>
</body>
</html>
