<%--
  Created by IntelliJ IDEA.
  User: John
  Date: 2019/7/4
  Time: 18:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree"  lay-filter="test">
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:;">消息管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="/message/to_receive_list">收件箱（原生JS生成）</a></dd>
                        <dd><a href="/message//to_receive_list_layui">收件箱（LayUI表格）</a></dd>
                        <dd><a href="/message/to_send_list">发件箱</a></dd>
                        <dd><a href="/message/to_send">写消息</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">账户管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="/user/show_user">我的资料</a></dd>
                    </dl>
                </li>
            </ul>
        </div>
    </div>
</div>
