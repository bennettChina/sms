<%--
  Created by IntelliJ IDEA.
  User: John
  Date: 2019/7/4
  Time: 18:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../common/header.jsp"/>
<jsp:include page="../common/menu.jsp"/>
<div class="layui-body" style="top: 70px; padding: 0 10px;">

    <span class="layui-breadcrumb">
      <a href="/to_main">首页</a>
      <a href="/message/to_receive_list">消息管理</a>
      <a><cite>发消息</cite></a>
    </span>
    <!-- 内容主体区域 -->
    <form id="sendMessageForm" class="layui-form"  style="margin-top: 20px;">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">
                <c:if test="${from_page == 'send'}">
                    接收人：
                </c:if>
                <c:if test="${from_page == 'receive'}">
                    发送人：
                </c:if>
            </label>
            <div class="layui-input-inline">
                <label class="layui-form-label">
                    <c:if test="${from_page == 'send'}">
                        ${toUser.nickName}
                    </c:if>
                    <c:if test="${from_page == 'receive'}">
                        ${fromUser.nickName}
                    </c:if>
                </label>
            </div>
        </div>

        <div class="layui-inline">
            <label class="layui-form-label">消息标题：</label>
            <div class="layui-form-label" style="padding-left: 5px;">
                ${message.subject}
            </div>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">消息内容：</label>
            <div class="layui-input-block">
                <div style="border: 1px solid silver; min-height: 300px;">
                    ${message.content}
                </div>
            </div>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">附件：</label>
            <a download href="${basePath}${message.attachment}">${message.attachment}</a>
        </div>
    </div>
    </form>
</div>
<jsp:include page="../common/footer.jsp"/>