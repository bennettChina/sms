<%--
  Created by IntelliJ IDEA.
  User: John
  Date: 2019/7/4
  Time: 18:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/header.jsp"/>
<jsp:include page="../common/menu.jsp"/>
<div class="layui-body" style="top: 70px; padding: 0 10px;">

    <span class="layui-breadcrumb">
      <a href="/to_main">首页</a>
      <a href="/message/to_receive_list">消息管理</a>
      <a><cite>收件箱</cite></a>
    </span>
    <!-- 内容主体区域 -->
    <!-- table内容在footer.jsp中通过layui的表格插件生成 -->
    <table id="receiveMsgTable" class="layui-table" lay-even="" lay-skin="nob">
    </table>
</div>

<jsp:include page="../common/footer.jsp"/>