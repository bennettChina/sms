<%--
  Created by IntelliJ IDEA.
  User: John
  Date: 2019/7/4
  Time: 18:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../common/header.jsp"/>
<jsp:include page="../common/menu.jsp"/>
<div class="layui-body" style="top: 70px; padding: 0 10px;">

    <span class="layui-breadcrumb">
      <a href="/to_main">首页</a>
      <a href="/message/to_receive_list">消息管理</a>
      <a><cite>收件箱</cite></a>
    </span>
    <!-- 内容主体区域 -->
    <table class="layui-table" lay-even="" lay-skin="nob">
        <colgroup>
            <col width="70">
            <col width="100">
            <col>
            <col width="150">
            <col width="200">
        </colgroup>
        <thead>
        <tr>
            <th>状态</th>
            <th>来自</th>
            <th>标题</th>
            <th>操作</th>
            <th>时间</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<jsp:include page="../common/footer.jsp"/>
<script type="text/javascript">
    function loadReceiveMsg() {
        $.getJSON('/message/load_receive_msg', function(msgList){
            //console.log(msgList);
            // 先清空表格中现有数据
            $("tbody").empty();
            // 在循环用新数据生成表格 msgList:[] obj: {}
            // [{},{},{}...]
            $.each(msgList, function(idx, obj){
                var status = obj.status;
                var subject = obj.subject;
                var createtime = obj.createtime;
                var id = obj.id;
                var toId = obj.fromId;
                var fromNickName = obj.fromUser.nickName;

                // 生成tr
                var tr = $("<tr>");
                // 第一列：图标状态
                // 给tr添加图标状态td
                var td1 = $("<td>");
                if(status == '1') {
                   td1.append("<img src=\"/static/images/sms_readed.png\" />");
                } else if(status == '2') {
                    td1.append("<img src=\"/static/images/sms_unReaded.png\" />");
                }
                tr.append(td1);

                // 第二列： 消息标题
                var td1_5 = $("<td>");
                td1_5.append(fromNickName);
                tr.append(td1_5);

                // 第二列： 消息标题
                var td2 = $("<td>");
                td2.append("<a href=\"/message/to_read/"+id+"/receive\">"+subject+"</a>");
                tr.append(td2);

                // 第三列： 操作列
                var td3 = $("<td>");
                td3.append("<a href=\"javascript:del_msg("+id+");\">删除</a>\n" +
                    "                    &nbsp;&nbsp;\n" +
                    "            <a href=\"/message/to_reply/"+id+"/"+toId+"?toNickName="+fromNickName+"\">回信</a>");
                tr.append(td3);

                // 第四列： 消息接收时间
                var td4 = $("<td>");
                td4.append(createtime);
                tr.append(td4);

                // 将我们动态生成的tr添加到tbody标签中
                $("tbody").append(tr);
            });
        });
    }
    // 页面加载时立即去load一次数据
    loadReceiveMsg();
    // 每隔5秒执行一次loadReceiveMsg方法
    setInterval(loadReceiveMsg , 5000);
</script>