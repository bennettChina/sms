package com.lanou3g.ssm.utils;

public class StringUtil {

    /**
     * 判断字符串是否为空
     * null、""都返回true, "    ", 返回false
     * @param str
     * @return
     */
    public static boolean isEmpty(String str) {
        return str == null || "".equals(str);
    }

    /**
     * 判断字符串是否为空
     * null、""、"    "都返回true
     * @param str
     * @return
     */
    public static boolean isBlank(String str) {
        return isEmpty(str) || "".equals(str.trim());
    }

}
