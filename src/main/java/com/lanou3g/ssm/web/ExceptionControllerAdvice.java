package com.lanou3g.ssm.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class ExceptionControllerAdvice {

    /**
     * 定义全局异常处理
     * @param ex
     * @return
     */
    @ExceptionHandler
    public ResponseEntity<String> handle(Exception ex) {
        log.error("出错了", ex);
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
}
