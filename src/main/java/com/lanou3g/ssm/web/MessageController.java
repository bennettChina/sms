package com.lanou3g.ssm.web;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lanou3g.ssm.bean.LayUIResponseStatus;
import com.lanou3g.ssm.bean.Message;
import com.lanou3g.ssm.bean.User;
import com.lanou3g.ssm.service.MessageService;
import com.lanou3g.ssm.service.UserService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/message")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @Autowired
    private UserService userService;

    /**
     * 去往收件箱页面（原生JS生成表格）
     * @return
     */
    @RequestMapping("/to_receive_list")
    public String toReceiveMessageList() {
        return "/message/receive_message_list";
    }

    /**
     * 去往收件箱页面(LayUI表格)
     * @return
     */
    @RequestMapping("/to_receive_list_layui")
    public String toReceiveMessageListForLayUI() {
        return "/message/receive_message_list_layui";
    }

    /**
     * 异步返回收件箱数据
     * @param session
     * @return
     */
    @RequestMapping("/load_receive_msg_layui")
    @ResponseBody
    public LayUIResponseStatus loadReceiveMessageListForLayUI(@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum, HttpSession session) {
        LayUIResponseStatus status = LayUIResponseStatus.ok();
        // 使用PageHelper插件进行分页
        PageHelper.startPage(pageNum, pageSize);
        // 封装分页参数和当前页数据到返回对象中
        Page<Message> pager = (Page<Message>) messageService.queryReceiveMessage(session);
        status.setData(pager);
        status.setCount((int)pager.getTotal());
        return status;
    }

    @RequestMapping("/load_receive_msg")
    @ResponseBody
    public List<Message> loadReceiveMessageList(HttpSession session) {
        return  messageService.queryReceiveMessage(session);
    }

    /**
     * 发件箱
     * @param session
     * @param model
     * @return
     */
    @RequestMapping("/to_send_list")
    // 表示只处理get请求
//    @RequestMapping(value = "/to_main", method = RequestMethod.GET)
    public String toSendMessageList(HttpSession session, Model model) {
        List<Message> messageList = messageService.querySendMessage(session);
        model.addAttribute("messageList", messageList);
        return "message/send_message_list";
    }

    /**
     * 去消息发送页
     * @param model 存放需要传到消息发送页的参数(接收人列表)
     * @param session
     * @return
     */
    @RequestMapping("/to_send")
    public String toSendPage(Model model, HttpSession session) {
        List<User> userList = userService.queryAllUserWithoutMe(session);
        model.addAttribute("userList", userList);
        return "message/send";
    }

    /**
     * 发送消息
     * @param message   消息对象
     * @param file  上传的附件
     * @param req
     * @return
     */
    @RequestMapping("/send")
    public String sendMessage(Message message, @RequestParam("myFile") MultipartFile file, HttpServletRequest req) {
        messageService.sendMessage(message, file, req);
        return "forward:/message/to_send_list";
    }

    /**
     * 去往读消息界面
     * @param id    消息ID
     * @param fromPage  从哪个页面点进来的
     *                  (如果从发件箱点进来，则要展示接收人；如果从收件箱点进来，则要展示发送人)
     * @param model 要往读消息页面传的参数
     * @return
     */
    @RequestMapping("/to_read/{id}/{from_page}")
    public String toReadPage(@PathVariable("id") Integer id, @PathVariable("from_page") String fromPage, Model model) {
        messageService.toReadPage(id, fromPage, model);
        return "message/read";
    }

    // 这种纯页面跳转的方法就可以直接通过配置一个视图控制器代替
    /*@RequestMapping("/to_main")
    public String toMainPage() {
        return "main";
    }*/

    /**
     * 删除消息
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping("/del")
    public LayUIResponseStatus deleteMsg(@RequestParam("id") Integer id) {
        try {
            messageService.deleteMessageById(id);
            return LayUIResponseStatus.ok();
        } catch (Exception e) {
            return  LayUIResponseStatus.error();
        }
    }

    /**
     * 去回信页
     * @return
     */
    @RequestMapping("/to_reply/{id}/{toId}")
    public String toReplyPage(@PathVariable("id")Integer id, @PathVariable("toId") Integer toId, @RequestParam("toNickName") String toNickName, Model model) {
        model.addAttribute("nickName", toNickName);
        return "/message/reply";
    }
}
