package com.lanou3g.ssm.mapper;

import com.lanou3g.ssm.bean.User;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface UserMapper {
    List<User> selectUser(User condition);

    void insertUser(User user);

    void updateUser(User user);
}