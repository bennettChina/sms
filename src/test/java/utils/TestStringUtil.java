package utils;

import com.lanou3g.ssm.utils.StringUtil;
import org.junit.Assert;
import org.junit.Test;

public class TestStringUtil {

    @Test
    public void testIsEmpty() {
        Assert.assertTrue("应该是真", StringUtil.isEmpty(null));
        Assert.assertTrue("应该是真", StringUtil.isEmpty(""));
        Assert.assertFalse("应该是假", StringUtil.isEmpty("   "));
        Assert.assertFalse("应该是假", StringUtil.isEmpty("	"));
    }

    @Test
    public void testIsBlank() {
        Assert.assertTrue("应该是真", StringUtil.isBlank(null));
        Assert.assertTrue("应该是真", StringUtil.isBlank(""));
        Assert.assertTrue("应该是真", StringUtil.isBlank("    "));
        Assert.assertTrue("应该是真", StringUtil.isBlank("	"));
    }
}
